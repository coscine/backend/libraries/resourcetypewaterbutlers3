﻿using Coscine.Configuration;
using Coscine.ResourceTypeBase;
using NUnit.Framework;
using System.Collections.Generic;

namespace Coscine.ResourceTypeWaterbutlerS3.Test
{
    [TestFixture]
    public class ResourceTypeWaterbutlerS3Tests
    {
        private readonly ResourceTypeConfigurationObject resourceTypeConfiguration = new ResourceTypeConfigurationObject();
        private readonly IConfiguration configuration = new ConsulConfiguration();

        [OneTimeSetUp]
        public void Setup()
        {
            resourceTypeConfiguration.Config = new Dictionary<string, string>
            {
                { "resourceUrl", "https://s3.rwth-aachen.de/" }
            };
        }

        [OneTimeTearDown]
        public void End()
        {
        }

        [Test]
        public void TestConstructor()
        {
            _ = new ResourceTypeWaterbutlerS3("s3", configuration, resourceTypeConfiguration);
        }
    }
}
