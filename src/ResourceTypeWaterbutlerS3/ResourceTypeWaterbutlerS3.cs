﻿using Amazon.S3;
using Amazon.S3.Model;
using Coscine.Configuration;
using Coscine.ResourceTypeBase;
using Coscine.WaterbutlerHelper;
using Coscine.WaterbutlerHelper.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Coscine.ResourceTypeWaterbutlerS3
{
    public class ResourceTypeWaterbutlerS3 : ResourceTypeDefinition
    {
        private readonly WaterbutlerInterface _waterbutlerInterface;

        private readonly AmazonS3Config _amazonConfig;

        public ResourceTypeWaterbutlerS3(string name, IConfiguration gConfig, ResourceTypeConfigurationObject resourceTypeConfiguration) : base(name, gConfig, resourceTypeConfiguration)
        {
            _waterbutlerInterface = new WaterbutlerInterface(Configuration, new DataSourceService(new HttpClient()));
            _amazonConfig = new AmazonS3Config
            {
                ServiceURL = gConfig.GetStringAndWait("coscine/global/ecs/s3_endpoint"),
                ForcePathStyle = true
            };
        }

        public override async Task<long> GetResourceQuotaUsed(string id, Dictionary<string, string> options = null)
        {
            using (var client = new AmazonS3Client(options["accessKey"], options["secretKey"], _amazonConfig))
            {
                long totalFileSize = 0;
                long fileCount = 0;
                var listRequest = new ListObjectsRequest()
                {
                    BucketName = options["bucketname"]
                };

                ListObjectsResponse listResponse;
                do
                {
                    listResponse = await client.ListObjectsAsync(listRequest);
                    fileCount += listResponse.S3Objects.Count();
                    totalFileSize += listResponse.S3Objects.Sum(x => x.Size);
                    listRequest.Marker = listResponse.NextMarker;

                } while (listResponse.IsTruncated);

                return totalFileSize;
            }
        }

        public override async Task<List<ResourceEntry>> ListEntries(string id, string prefix, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(prefix, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{prefix}\".");
            }

            var entries = new List<ResourceEntry>();

            foreach (var info in infos)
            {
                entries.Add(new ResourceEntry(info.Path, info.IsFile, info.Size == null ? 0 : (long)info.Size, null, null, info.Created == null ? new DateTime() : (DateTime)info.Created, info.Modified == null ? new DateTime() : (DateTime)info.Modified));
            }

            return entries;
        }

        public override async Task<ResourceEntry> GetEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos.First().IsFile)
            {
                throw new Exception("Not a file.");
            }

            return new ResourceEntry(key, infos.First().IsFile, (long)infos.First().Size, null, null, infos.First().Created == null ? new DateTime() : (DateTime)infos.First().Created, infos.First().Modified == null ? new DateTime() : (DateTime)infos.First().Modified);
        }

        public override async Task StoreEntry(string id, string key, Stream body, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);
            long contentLength = (long)Convert.ToDouble(options["ContentLength"]);

            // Not found, upload new
            if (infos == null)
            {
                var filename = key.Substring(key.LastIndexOf("/") + 1);
                var rootPath = key.Substring(0, key.Length - filename.Length);

                HandleResponse(await _waterbutlerInterface.UploadFileAsync(rootPath, filename, Name, authHeader, body, contentLength));

            }
            else
            {
                // update existing file
                // Not a file
                if (infos.Count > 1 || !infos.First().IsFile)
                {
                    throw new Exception("Not an updateable or new file.");
                }

                HandleResponse(await _waterbutlerInterface.UploadObjectAsync(infos.First(), authHeader, body, contentLength));
            }
        }

        public override async Task DeleteEntry(string id, string key, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);
            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under {key}.");
            }

            // Not a file
            if (infos.Count > 1 || !infos.First().IsFile)
            {
                throw new Exception($"Not a file.");
            }

            HandleResponse(await _waterbutlerInterface.DeleteObjectAsync(infos.First(), authHeader));
        }

        public override async Task<Stream> LoadEntry(string id, string key, string version = null, Dictionary<string, string> options = null)
        {
            var authHeader = _waterbutlerInterface.BuildAuthHeader(Name, options);

            var infos = await _waterbutlerInterface.GetObjectInfoAsync(key, Name, authHeader);

            // Not found
            if (infos == null)
            {
                throw new Exception($"Found nothing in waterbutler under \"{key}\".");
            }

            // Not a file
            if (infos.Count > 1 || !infos.First().IsFile)
            {
                throw new Exception("Not a file.");
            }

            // Do not dispose the response!
            // The stream is later accessed by the MVC, to deliver the file.
            // When disposed, the stream becomes invalid and can't be read.
            var response = await _waterbutlerInterface.DownloadObjectAsync(infos.First(), authHeader);
            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStreamAsync();
            }

            return null;
        }

        protected void HandleResponse(HttpResponseMessage httpResponseMessage)
        {
            var statuscode = (int)httpResponseMessage.StatusCode;
            if (statuscode == 403)
            {
                throw new RTDForbiddenException();
            }
            else if (statuscode == 404)
            {
                throw new RTDNotFoundException();
            }
            else if (statuscode >= 400 && statuscode < 600)
            {
                throw new RTDBadRequestException();
            }
        }

        public override async Task<Uri> GetEntryDownloadUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return await Task.Run(() =>
            {
                using (var client = new AmazonS3Client(options["accessKey"], options["secretKey"], _amazonConfig))
                {
                    var presignedUrl = client.GetPreSignedURL(new GetPreSignedUrlRequest()
                    {
                        BucketName = options["bucketname"],
                        Key = key,
                        Verb = HttpVerb.GET,
                        Protocol = Protocol.HTTP,
                        // For now, expiry of a day is set, but this might be up to debate
                        Expires = DateTime.UtcNow.AddHours(24)
                    });
                    return new Uri(presignedUrl);
                }
            });
        }

        public override async Task<Uri> GetEntryStoreUrl(string key, string version = null, Dictionary<string, string> options = null)
        {
            return await Task.Run(() =>
            {
                using (var client = new AmazonS3Client(options["accessKey"], options["secretKey"], _amazonConfig))
                {
                    var presignedUrl = client.GetPreSignedURL(new GetPreSignedUrlRequest()
                    {
                        BucketName = options["bucketname"],
                        Key = key,
                        Verb = HttpVerb.PUT,
                        Protocol = Protocol.HTTP,
                        // For now, expiry of a day is set, but this might be up to debate
                        Expires = DateTime.UtcNow.AddHours(24)
                    });
                    return new Uri(presignedUrl);
                }
            });
        }
    }
}
